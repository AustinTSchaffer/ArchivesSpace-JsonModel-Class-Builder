"""
These classes are used to generate source code based on the classes from the
`schema_classes` module.
"""

from pycodefactory import enums
from pycodefactory.class_structures import (
    python_classes,
    properties,
    property_types,
    dependencies
)

GETTER_TEMPLATE = """
def {public_private}get_{property_name}(self) -> {property_type}:
    {docstring}
    return self.data.get("{property_name}")
"""

SETTER_TEMPLATE = """
def {public_private}set_{property_name}(self, value: {property_type}):
    {docstring}
    self.data.set("{property_name}", value)
    return self
"""

class PythonSourceCodeFormatter(object):
    def __init__(self, indent_type: enums.IndentType = enums.IndentType.SPACES_4,
                 show_return_type=True, show_param_type=True):
        self.indent_string = indent_type
        self.show_return_type = bool(show_return_type)
        self.show_param_type = bool(show_param_type)

    def dict_style_class_property(self, 
        py_class_property: properties.PythonClassProperty) -> list:
        """
        Outputs the source code for a given property.

        NOTE These way these properties are generated are dependant on the
        class inheriting from dict.
        """
        prop = py_class_property

        # TODO Check to see if property name is reserved word
        fixed_property_name = prop.property_name

        code.append('@property')
        code.append('def %s(self)%s:' % (
            fixed_property_name,
            (' -> ' + prop.type_name)
            if self.show_return_type and prop.type_name is not None else
            ''
        ))
        code.indent_level = 2
        code.append("return self.get('%s', None)" % prop.property_name)

        if prop.readonly:
            return code.lines
        
        code.blank_line()
        
        code.indent_level = 1
        code.append('@%s.setter' % fixed_property_name)
        code.append('def %s(self, value%s):' % (
            fixed_property_name,
            (': ' + prop.type_name)
            if self.show_param_type and prop.type_name is not None else
            ''
        ))
        code.indent_level = 2
        # TODO Validations? Type Checking?
        code.append("self['%s'] = value" % prop.property_name)

        code.blank_line()

        code.indent_level = 1
        code.append('@%s.delete' % fixed_property_name)
        code.append('def %s(self):' % fixed_property_name)
        code.indent_level = 2
        code.append("del self['%s']" % prop.property_name)

        return code.lines

    def dict_style_class(self, py_class: python_classes.PythonClass) -> list:
        """
        Outputs source code as a list of strings, each string being a
        new line.
        """
        code = SourceCode(self.indent_string)

        # TODO Imports can be much better
        for include in py_class.get_dependencies():
            code.append('import .%s' % include)
        
        code.blank_line()
        code.blank_line()
        code.append('class %s(%s):' % (
            py_class.classname,
            py_class.superclassname,
        ))

        for prop in py_class.properties:
            code.blank_line()
            for line in self.dict_style_class_property(prop):
                code.append(line)
            code.blank_line()

        return code.lines
