from enum import Enum

class IndentType(Enum):
    SPACES_4 = '    '
    SPACES_2 = '  '
    SPACES_1 = ' '
    TABS = '\t'

class AccessLevelModifier(Enum):
    """
    Specifies the readability and writability of a Python class property.
    """
    FULL = 0
    PRIVATE_SET = 1
    PRIVATE_GET = 2
    PRIVATE_GET_PRIVATE_SET = 3
    READONLY = 4
    WRITEONLY = 5
    UNUSED = 6
