class PythonClassPropertyType(object):
    def __init__(self, type_name: str, python_type: type = None):
        self.type_name = type_name
        self.python_type = python_type
