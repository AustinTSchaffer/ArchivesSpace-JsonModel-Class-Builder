from pycodefactory.class_structures import (
    properties as _properties,
    dependencies as _dependencies,
)


class PythonClass(object):
    def __init__(self):
        self.data = {}

    def get_class_name(self) -> str:
        """
        Specifies the class's name as a string.
        """
        return self.data.get('_class_name')

    def set_class_name(self, value: str):
        """
        Specifies the class's name as a string.
        """
        assert isinstance(value, str)
        self.data['_class_name'] = value
        return self

    class_name = property(
        fget=get_class_name, fset=set_class_name, doc=(
            "Specifies the class's name as a string."
        )
    )

    def get_superclass_names(self) -> set:
        """
        Specifies a list of strings to use as the superclasses for this class.
        """
        return self.data.setdefault(
            '_superclass_names',
            set([object.__name__])
        )

    def set_superclass_names(self, values):
        """
        Specifies a list of strings to use as the superclasses for this class.
        """
        scns = self.get_superclass_names()
        scns.clear()
        for scn in values:
            assert isinstance(scn, str)
            scns.add(scn)
        return self

    superclass_names = property(
        fget=get_superclass_names, fset=set_superclass_names, doc=(
            "Specifies a list of strings to use as the superclasses for this "
            "class."
        )
    )

    def get_dependencies(self) -> set:
        """
        Specifies a set of dependencies that the class needs to import
        """
        return self.data.setdefault('_dependencies', set())

    def set_dependencies(self, values):
        """
        Specifies a set of dependencies that the class needs to import.
        """
        deps = self.get_dependencies()
        deps.clear()
        for dep in values:
            assert isinstance(dep, _dependencies.PythonClassDependency)
            deps.add(dep)
        return self

    dependencies = property(
        fget=get_dependencies, fset=set_dependencies, doc=(
            "Specifies a set of dependencies that the class needs to import."
        )
    )

    def get_properties(self) -> set:
        """
        Specifies a set of properties that apply to the class.
        """
        return self.data.setdefault('_properties', set())

    def set_properties(self, values):
        """
        Specifies a set of properties that apply to the class.
        """
        props = self.get_properties()
        props.clear()
        for prop in props:
            assert isinstance(prop, _properties.PythonClassProperty)
            props.add(prop)
        return self

    properties = property(
        fget=get_properties, fset=set_properties, doc=(
            "Specifies a set of properties that apply to the class."
        )
    )
