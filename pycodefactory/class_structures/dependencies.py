class PythonClassDependency(object):
    """
    Specifies a dependancy that needs to be imported for a file, when the
    file only contains one class.
    """

    def __init__(self, import_string: str, from_string: str = None,
                 as_string: str = None):
        self.import_string = import_string
        self.from_string = from_string
        self.as_string = as_string

    def __key(self):
        return (self.from_string, self.import_string, self.as_string)

    def __eq__(self, other):
        return (
            type(self) == type(other) and
            self.__key() == other.__key()
        )

    def __hash__(self):
        return hash(self.__key)


def format_dependency(dependency: PythonClassDependency) -> str:
    """
    Outputs import as a string.
    """
    return '%s%s%s' % (
        'from %s ' % dependency.from_string
        if dependency.from_string else '',

        'import %s' % dependency.import_string
        if dependency.import_string else '',

        ' as %s' % dependency.as_string
        if dependency.as_string else '',
    )


def format_dependencies(dependencies: list) -> list:
    """
    Outputs imports as a list of strings, each entry representing a new line.
    """
    return list(map(format_dependency, dependencies))
