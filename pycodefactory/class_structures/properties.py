from pycodefactory import enums
from pycodefactory.class_structures import property_types


class PythonClassProperty(object):
    """
    Encapsulates an instance property for a Python class.
    """
    def __init__(self):
        self.data = {}
        self.access_level_modifier = enums.AccessLevelModifier.FULL


    def get_property_type(self) -> property_types.PythonClassPropertyType:
        """Type information for this property."""
        return self.data.get('property_type')
    
    def set_property_type(self, value: property_types.PythonClassPropertyType):
        """Type information for this property."""
        assert isinstance(value, property_types.PythonClassPropertyType)
        self.data['property_type'] = value
        return self

    property_type = property(
        fget=get_property_type, fset=set_property_type, doc=(
            "Type information for this property."
        )
    )

    def get_property_name(self) -> str:
        """The name used to access the property from the class. """
        return self.data.get('property_name')

    def set_property_name(self, value: str):
        """The name used to access the property from the class. """
        assert isinstance(value, str)
        self.data['property_name'] = value

    property_name = property(
        fget=get_property_name, fset=set_property_name, doc=(
            "The name used to access the property from the class."
        )
    )

    def get_access_level_modifier(self) -> enums.AccessLevelModifier:
        """Specifies the access level modifier for this property. """
        return self.data.get('access_level_modifier')

    def set_access_level_modifier(self, value: enums.AccessLevelModifier):
        """Specifies the access level modifier for this property. """
        assert isinstance(value, enums.AccessLevelModifier)
        self.data['access_level_modifier'] = value

    access_level_modifier = property(
        fget=get_access_level_modifier, fset=set_access_level_modifier, doc=(
            "Specifies the access level modifier for this property."
        )
    )

    def __key(self):
        return (self.property_name)

    def __eq__(self, other):
        return (
            type(self) == type(other) and
            self.__key() == other.__key()
        )

    def __hash__(self):
        return hash(self.__key())
