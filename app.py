import os

from aspace import client

from aspaceschemagenerator import schema_classes
from aspaceschemagenerator import schema_reports
from aspaceschemagenerator import source_code

OUTPUT_DIR = '/home/austin/sample_output/'
ASPACE_INSTANCE = 'http://localhost:8089'

def main():
    CLIENT = client.ASpaceClient(
        ASPACE_INSTANCE,
        auto_auth = False,
    )

    source_code_formatter = source_code.PythonSourceCodeFormatter()
    schemas = [
        schema_classes.SchemaRecord(schema_name, schema_info)
        for schema_name, schema_info in 
        CLIENT.get('/schemas').json().items()
    ]

    for schema in schemas:
        # TODO As mentioned in the source code library, this could be much
        # smarter, so it could be used in other projects

        print('Processing Schema:', schema.jsonmodel_name)
        class_info = source_code.PythonClass(schema)
        python_source_code = source_code_formatter.dict_style_class(class_info)

        # TODO CLI for output directory and source aspace instance. 
        os.makedirs(OUTPUT_DIR, exist_ok=True)
        filename = os.path.join(OUTPUT_DIR, '%s.py' % schema.jsonmodel_name)
        file = open(filename, 'w')
        for line in python_source_code:
            file.write(line)
            file.write(os.linesep)
        file.close()
        
    print('Done.')

if __name__ == '__main__':
    main()
