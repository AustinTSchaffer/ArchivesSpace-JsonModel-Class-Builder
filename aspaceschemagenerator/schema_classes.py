from aspaceschemagenerator import functions


class SchemaProperty(object):
    """
    Parses all the properties from the "properties" property of every schema
    record from the /schemas ArchivesSpace endpoint.  
    """
    def __init__(self, property_name: str, property_info: dict):
        self.property_name = property_name

        _info = dict.copy(property_info)

        self.type = functions.getdel(_info, 'type')
        self.subtype = functions.getdel(_info, 'subtype')
        self.properties = functions.getdel(_info, 'properties')

        self.default = functions.getdel(_info, 'default')
        self.readonly = functions.getdel(_info, 'readonly')
        self.enum = functions.getdel(_info, 'enum')
        self.dynamic_enum = functions.getdel(_info, 'dynamic_enum')

        self.items = functions.getdel(_info, 'items')
        self.min_items = functions.getdel(_info, 'minItems')
        self.additional_items = functions.getdel(_info, 'additionalItems')

        self.min_length = (
            functions.getdel(_info, 'minLength') or
            functions.getdel(_info, 'minlength')
        )
        
        self.max_length = (
            functions.getdel(_info, 'maxLength') or
            functions.getdel(_info, 'maxlength')
        )

        self.pattern = functions.getdel(_info, 'pattern')
        self.required = functions.getdel(_info, 'required')
        self.ifmissing = functions.getdel(_info, 'ifmissing')
        
        assert len(_info) <= 0, (
            'Please parse remaining properties in SchemaProperty: ' +
            ', '.join(_info)
        )

class SchemaRecord(object):
    """
    Parses the properties from all schema records from the /schemas
    ArchivesSpace endpoint.
    """
    def __init__(self, jsonmodel_name: str, jsonmodel: dict):
        self.jsonmodel_name = jsonmodel_name
        
        _jsonmodel = dict.copy(jsonmodel)
        
        self.type = functions.getdel(_jsonmodel, 'type')
        self.subtype = functions.getdel(_jsonmodel, 'subtype')
        self.parent = functions.getdel(_jsonmodel, 'parent')

        self.properties = [
            SchemaProperty(prop_name, prop_info)
            for prop_name, prop_info in
            functions.getdel(_jsonmodel, 'properties').items()
        ]

        self.uri = functions.getdel(_jsonmodel, 'uri')
        self.version = functions.getdel(_jsonmodel, 'version')
        self.dollarschema = functions.getdel(_jsonmodel, '$schema')
        self.validations = functions.getdel(_jsonmodel, 'validations')

        assert len(_jsonmodel) <= 0, ( 
            'Please parse remaining properties in SchemaRecord: ' +
            ', '.join(_jsonmodel)
        )
