
def getdel(d: dict, key):
    """
    Gets the specified key from the dict, then deletes it. If key does not
    exist in the dict, then this returns `None`.
    """
    if key in d:
        val = d[key]
        del d[key]
        return val
    return None
