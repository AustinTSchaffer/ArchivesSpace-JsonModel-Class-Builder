from aspace import client


def distinct_schema_info_keys(CLIENT: client.ASpaceClient) -> list:
    """
    Returns a set of strings. Each corresponds to a key that is used to 
    describe at least one schema from the /schemas ArchivesSpace endpoint.
    """
    return set([
        property_name
        for _, schemainfo in CLIENT.get('/schemas').json().items()
        for property_name, _ in schemainfo.items()
    ])

def distinct_property_info_keys(
    CLIENT: client.ASpaceClient) -> list:
    """
    Returns a set of strings. Each corresponds to a key that is used to 
    describe at least one property of a schema from the /schemas 
    ArchivesSpace endpoint.
    """
    return set([
        property_name
        for _, schemainfo in CLIENT.get('/schemas').json().items()
        for _, propinfo in schemainfo['properties'].items()
        for property_name, _ in propinfo.items()
    ])
