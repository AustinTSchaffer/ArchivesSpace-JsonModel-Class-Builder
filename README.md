# ArchivesSpace JSON Model Builder

This is a meta-project, designed to build classes for the aspace-client Python
package, based on the /schemas endpoint of the ArchivesSpace API.

- [aspace-client on PyPI](https://pypi.org/project/aspace-client/)
- [aspace-client on GitHub](https://github.com/AustinTSchaffer/ArchivesSpace-Python-Client)

## Installation

```bash
virtualenv --python=python3 venv 
source venv/bin/activate
pip install -r requirements.txt
```

